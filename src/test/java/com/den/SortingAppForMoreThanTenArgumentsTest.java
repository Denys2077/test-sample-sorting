package com.den;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppForMoreThanTenArgumentsTest {

	protected SortingApp sort = new SortingApp();
	
	private int[] actual;
	private int[] expected;
	
	public SortingAppForMoreThanTenArgumentsTest(int[] actual, int[] expected) {
		this.actual = actual;
		this.expected = expected;
	}
	
	@Parameters
	public static Collection<Object[]> data() {
		Object[][] objects = {
				{new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}, new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}},
				{new int[] {5, 1, -3, 2, 0, -4, 6, 8, 4, -8, -9, 10, -15, -100, -85, 85}, new int[] {-100, -85, -15, -9, -8, -4, -3, 0, 1, 2, 4, 5, 6, 8, 10, 85}},
				{new int[] {3, 5, 8, 9, 0, -2, -3, -1, 0, 4, 2, 7}, new int[] {-3, -2, -1, 0, 0, 2, 3, 4, 5, 7, 8, 9}},
				{new int[] {13, 9, 5, 11, 2, 7, 1, 12, 3, 6, 8, 4, 0, 10, 15, 14}, new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}}
		};
		return Arrays.asList(objects);
	}
	
	@Test
	public void testOneElement() {
		sort.sorting(actual);
		assertArrayEquals(expected, actual);
	}
}
