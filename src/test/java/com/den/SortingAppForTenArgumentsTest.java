package com.den;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppForTenArgumentsTest {

protected SortingApp sort = new SortingApp();
	
	private int[] actual;
	private int[] expected;
	
	public SortingAppForTenArgumentsTest(int[] actual, int[] expected) {
		this.actual = actual;
		this.expected = expected;
	}
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
			{new int[] {5, 1, -3, 2, 0, -4, 6, 8, 4, -8}, new int[] {-8, -4, -3, 0, 1, 2, 4, 5, 6, 8}},
			{new int[] {3, 5, 8, -2, -3, -1, 0, 4, 2, 7}, new int[] {-3, -2, -1, 0, 2, 3, 4, 5, 7, 8}},
			{new int[] {9, 5, 2, 7, 1, 3, 6, 8, 4, 0}, new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}}
		});
	}
	
	@Test
	public void testOneElement() {
		sort.sorting(actual);
		assertArrayEquals(expected, actual);
	}
}
