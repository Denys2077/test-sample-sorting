package com.den;

import org.junit.Assert;
import org.junit.Test;

public class SortingAppForZeroArgumentsTest {

	protected SortingApp sort = new SortingApp();
	
	@Test(expected = IllegalArgumentException.class)
	public void testException() {
		sort.sorting(null);
	}
	
	@Test
	public void testZeroArguments() {
		int[] actual = {};
		int[] expected = {};
		sort.sorting(actual);
		Assert.assertArrayEquals(expected, actual);
	}
}
