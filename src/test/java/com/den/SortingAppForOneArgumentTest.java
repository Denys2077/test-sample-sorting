package com.den;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppForOneArgumentTest {

	protected SortingApp sort = new SortingApp();
	
	private int[] actual;
	private int[] expected;
	
	public SortingAppForOneArgumentTest(int[] actual, int[] expected) {
		this.actual = actual;
		this.expected = expected;
	}
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
			{new int[] {1}, new int[] {1}},
			{new int[] {5}, new int[] {5}},
			{new int[] {3}, new int[] {3}},
			{new int[] {-2}, new int[] {-2}},
			{new int[] {0}, new int[] {0}},
		});
	}
	
	@Test
	public void testOneElement() {
		sort.sorting(actual);
		assertArrayEquals(expected, actual);
	}
}
