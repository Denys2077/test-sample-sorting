package com.den;

import java.util.Arrays;
import java.util.Scanner;

public class SortingApp {

	public static void main(String[] args) {
		SortingApp sort = new SortingApp();
		System.out.print("Enter array's length: ");
		int length = scan.nextInt();
		int[] arr = new int[length];
		System.out.println("Please, enter elements of the array!");
		for(int i = 0; i < length; i++) {
			System.out.print("[" + i + "] = ");
			arr[i] = scan.nextInt();
		}
		System.out.println("Array before sorting: " + Arrays.toString(arr));
		sort.sorting(arr);
		System.out.println("Array after sorting: " + Arrays.toString(arr));
	}
	
	private static final Scanner scan = new Scanner(System.in);
	
	public void sorting(int[] arr) {
		if(arr == null) {
			throw new IllegalArgumentException();
		}
		Arrays.sort(arr);
	}
}
